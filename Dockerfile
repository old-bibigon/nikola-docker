FROM debian:10

COPY . /build

WORKDIR /build/

RUN apt-get update && \
    apt-get install -y pipenv openjdk-11-jre-headless && \
    pipenv install --system --deploy --ignore-pipfile && \
    rm -rf /var/lib/apt/lists/*
